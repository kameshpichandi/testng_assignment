package com.tesng.assignment;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DemoWebShop {

	public static WebDriver driver;

	@BeforeMethod

	public void launchUrl() {
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
		driver.manage().window().maximize();
		driver.get("https://demowebshop.tricentis.com/login");
		driver.findElement(By.xpath("//a[text()='Log in']")).click();
		driver.findElement(By.id("Email")).sendKeys("kameshkam3094@gmail.com");
		driver.findElement(By.id("Password")).sendKeys("Kamesh@30");
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
	}

	@Test()

	public void addtoCart() {

		WebElement computer = driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Computers')]"));
		Actions ac = new Actions(driver);
		ac.moveToElement(computer).perform();
		driver.findElement(By.xpath("//ul[@class='top-menu']//a[normalize-space()='Desktops']")).click();

		WebElement sortBy = driver.findElement(By.id("products-orderby"));
		Select sc = new Select(sortBy);
		sc.selectByVisibleText("Price: Low to High");

		driver.findElement(By.xpath("(//div[@class='buttons']//input[@value='Add to cart'])[1]")).click();

		driver.findElement(By.id("add-to-cart-button-72")).click();
		driver.findElement(By.xpath("//span[contains(text(),'Shopping cart')]")).click();
		driver.findElement(By.id("termsofservice")).click();
		driver.findElement(By.id("checkout")).click();

		driver.findElement(By.xpath("//input[@onclick='Billing.save()']")).click();
		driver.findElement(By.xpath("//input[@onclick='Shipping.save()']")).click();
		driver.findElement(By.id("shippingoption_2")).click();
		driver.findElement(By.xpath("//input[@class='button-1 shipping-method-next-step-button']")).click();
		driver.findElement(By.xpath("//input[@class='button-1 payment-method-next-step-button']")).click();
		driver.findElement(By.xpath("//input[@class='button-1 payment-info-next-step-button']")).click();
		driver.findElement(By.xpath("//input[@value='Confirm']")).click();

		String actual = "Your order has been successfully processed!";
		String expected = driver.findElement(By.xpath("//strong[contains(text(),'Your order has been')]")).getText();
		Assert.assertEquals(expected, actual, "cheek the data");

	}

	@AfterMethod

	public void tearDown() {
		driver.findElement(By.xpath("//input[@value='Continue']")).click();
		driver.findElement(By.xpath("//a[text()='Log out']")).click();
		driver.quit();

	}

}
