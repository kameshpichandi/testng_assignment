package com.tesng.assignment;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class AnnotationinTestNG {

	@BeforeSuite

	public void beforeSuitMethod() {

		System.out.println("BeforeSuit Method");
	}

	@AfterSuite

	public void afterSuitMethod() {

		System.out.println("AfterSuite Method ");
	}

	@BeforeTest

	public void beforeTestMethod() {

		System.out.println("BeforeTest Method");
	}

	@AfterTest

	public void afterTestMethod() {

		System.out.println("AfterTest Method");
	}

	@BeforeGroups

	public void beforGroupMethod() {

		System.out.println("BeforGroup Method");
	}

	@AfterGroups

	public void afterGroupMethod() {

		System.out.println("AfterGroup Method");
	}

	@BeforeClass
	public void beforClassMethod() {

		System.out.println("BeforClass Method");
	}

	@AfterClass

	public void afterClassMethod() {

		System.out.println("AfterClass Method ");
	}

	@BeforeMethod

	public void beforMethod() {

		System.out.println("BeforMethod");
	}

	@AfterMethod

	public void afterMethod() {

		System.out.println("AfterMethod");
	}

	@Test

	public void mainTest1() {

		System.out.println("Main Test-1");
	}

	@Test
	public void mainTest2() {

		System.out.println("Main Test-2");
	}

}
