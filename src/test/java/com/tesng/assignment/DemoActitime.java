package com.tesng.assignment;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DemoActitime {

	// https://demo.actitime.com/login.do
	// Write a test script for checking "Please identify" text is present in the
	// webpage
	// Write a test script for checking if logo is present on the webpage
	WebDriver driver;

	@BeforeMethod

	public void setUp() {
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
		driver.manage().window().maximize();
		driver.get("https://demo.actitime.com/login.do");

	}

	@AfterMethod

	public void tearDown() {

		driver.quit();
	}

	@Test(priority = 1)

	public void testPresent() {
		boolean text = driver.findElement(By.xpath("//td[@id='headerContainer']")).isDisplayed();
		Assert.assertTrue(text);
	}

	@Test(priority = 2)

	public void logoTest() {

		boolean logo = driver.findElement(By.xpath("//div[@class='atLogoImg']")).isDisplayed();
		Assert.assertTrue(logo);
	}

}
